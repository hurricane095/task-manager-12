package ru.krivotulov.tm.api.service;

import ru.krivotulov.tm.enumerated.Status;
import ru.krivotulov.tm.model.Project;

import java.util.List;

public interface IProjectService {

    Project create(String name);

    Project create(String name, String description);

    Project add(Project project);

    List<Project> findAll();

    Project findOneById(String id);

    Project findOneByIndex(Integer index);

    Project updateById(String id, String name, String description);

    Project updateByIndex(Integer index, String name, String description);

    Project delete(Project project);

    Project deleteById(String id);

    Project deleteByIndex(Integer index);

    Project changeStatusById(String id, Status status);

    Project changeStatusByIndex(Integer index, Status status);

    void clear();

}
